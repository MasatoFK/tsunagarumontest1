$(function(){
    $('.global-nav-1 li').hover(function(){
        $("ul:not(:animated)", this).slideDown();
    }, function(){
        $("ul.category-list",this).slideUp();
    });
});

$(function() {
    var nav = $('.global');
    //表示位置
    var navTop = nav.offset().top+250;
    //ナビゲーションの高さ（シャドウの分だけ足してます）
    var navHeight = nav.height()+10;
    var showFlag = false;
    nav.css('top', -navHeight+'px');
    //ナビゲーションの位置まできたら表示
    $(window).scroll(function () {
        var winTop = $(this).scrollTop();
        if (winTop >= navTop) {
            if (showFlag == false) {
                showFlag = true;
                nav
                    .addClass('fixed')
                    .stop().animate({'top' : '0px'}, 200);
            }
        } else if (winTop <= navTop) {
            if (showFlag) {
                showFlag = false;
                nav.stop().animate({'top' : -navHeight+'px'}, 200, function(){
                    nav.removeClass('fixed');
                });
            }
        }
    });
});

$(function(){
  $('#tab-menu div').on('click', function(){
    if($(this).not('active')){
      // タブメニュー
      $(this).addClass('active').siblings('div').removeClass('active');
      // タブの中身
      var index = $('#tab-menu div').index(this);
      $('#tab-box section').eq(index).addClass('active').siblings('section').removeClass('active');
    }
  });
});
