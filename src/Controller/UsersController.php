<?php

namespace app\Controller;

use Cake\Event\Event;
use App\Controller\AppController;
use Cake\ORM\TableRegistry;


class UsersController extends AppController
{
    /**
     * AuthComponentを使用した認証処理
     */
    public function login()
    {

        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                return $this->redirect($this->Auth->redirectUrl());
            } else {
                $this->Flash->error(__('Username or password is incorrect'), [
                    'key' => 'auth'
                ]);
            }

            //$this->Session->setFlash(__('Invalid username or password, try again'));
        }
    }

    /**
     * @return \Cake\Network\Response|null
     */
    public function register() {
        $userTable = TableRegistry::get('Users');
        $user = $userTable->newEntity();
        if ($this->request->is('post')) {
            $password = $this->request->data('User.password');
            $email = $this->request->data('User.email');
            $username = $this->request->data('User.username');
            $user->set('email', $email);
            $user->set('password', $password);
            $user->set('username', $username);
            $createTime = date('Y-m-d H:i:s');
            $user->set('create_time', $createTime);
            $user->set('update_time',$createTime);

            if ($userTable->save($user)) {
                //$this->Session->setFlash('登録完了しました.');
                $this->Flash->set('The user has been saved.', [
                    'element' => 'success'
                ]);
                return $this->redirect('/');
            } else {
                $user->set('password', $password);
                //$this->Session->setFlash('登録に失敗しました, もう一度行なって下さい.');
                $this->Flash->set('登録に失敗しました, もう一度行なって下さい.', [
                    'element' => 'error'
                ]);
            }
        }
        $this->set(compact('user'));
    }

    public function logout()
    {
        $this->request->session()->destroy();
        return $this->redirect($this->Auth->logout());
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow();
    }


}