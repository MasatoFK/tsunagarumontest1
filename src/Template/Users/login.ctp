<?php
    $this->assign('title','登錄 | tsunagarumon');
?>

<main role="main" class="login">


<h1>登錄</h1>

<div class="pure-g" id="tab-menu">
	<div class="pure-u-1-2 active"><p>密碼登錄</p></div>
	<div class="pure-u-1-2"><p>短信登錄</p></div>
</div>


<div id="tab-box" class="loginform">
<section class="active">

	<form class="pure-form pure-g" method="post" accept-charset="utf-8" action="/users/login">
	<fieldset>

	<div class="pure-u-1">
	<p><input id="email" type="email" name='User[email]' placeholder="手機/郵箱/個性帳號" class="pure-input-1"></p>
	<p><input id="password" type="password" name='User[password]' placeholder="請輸入密碼" class="pure-input-1"></p>

	<label for="remember" class="pure-checkbox">
		<input id="remember" type="checkbox"> 自動登錄
	</label>

	<p><button type="submit" class="pure-button pure-input-1">登錄</button></p>
	</div>

	<div class="pure-u-1-2"><p><a href="./resetpassword.html">忘記密碼?</a></p></div>
	<div class="pure-u-1-2"><p class="text-right"><a href="/users/register">立即註冊</a></p></div>

	</fieldset>
	</form>
</section>


<section>

	<form class="pure-form pure-g">
	<fieldset>

	<div class="pure-u-1">
	<p><input id="email" type="email" placeholder="手機/郵箱/個性帳號" class="pure-input-1"></p>
	</div>

	<div class="pure-u-sm-2-3 pure-u-1-2">
		<input id="password" type="password" placeholder="短信驗證碼" class="pure-input-1">
	</div>

	<div class="pure-u-sm-1-3 pure-u-1-2">
		<input id="btnSendMsg" type="button" value="獲取手機驗證碼" class="pure-input-1 pure-button">
	</div>

	<div class="pure-u-1">
	<label for="remember" class="pure-checkbox">
		<input id="remember" type="checkbox"> 自動登錄
	</label>
	<p><button type="submit" class="pure-button pure-input-1">登錄</button></p>
	</div>

	<div class="pure-u-1"><p class="text-right"><a href="/users/register">立即註冊</a></p></div>

	</fieldset>
	</form>

</section>

</div>

</main>
