<?php
    $this->assign('title','註冊 | tsunagarumon');
?>

<!-- ************************************************************************  -->
<main role="main" class="login">

<h1>註冊</h1>

<div class="loginform">

<section>

	<form class="pure-form pure-g" method="post" accept-charset="utf-8" action="/users/register">
	<fieldset>

	<div class="pure-u-sm-7-8 pure-u-3-4">
		<p><input id="email" type="email" name='User[email]' placeholder="手機/email/帳號" class="pure-input-1"></p>
		<p><input id="username" type="username" name='User[username]' placeholder="手機/email/帳號" class="pure-input-1"></p>
	</div>

	<div class="pure-u-1">
	<p><input id="password" type="password" name='User[password]' placeholder="請設置 6~18 字母或數字的密碼" class="pure-input-1"></p>
	<p><input id="password" type="password" name='User[password2]' placeholder="￼驗證碼" class="pure-input-1"></p>
	<p><button type="submit" class="pure-button pure-input-1">註冊</button></p>
	
	</div>

	<div class="pure-u-1">
	<p><label for="remember" class="pure-checkbox">
			<input id="remember" type="checkbox"> 已閱讀,並同意用戶協議和隱私條款
		</label></p>
	</div>

	</fieldset>
	</form>


</section>

</div>


</main>
<!-- ************************************************************************  -->
