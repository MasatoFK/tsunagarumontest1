<?php
    $this->assign('title','首頁 | tsunagarumon');
?>

<main role="main" class="topindex">

<h1 class="text-hidden">tsunagarumon -首頁-</h1>

<div class="main-slick">
	<div><a href="./showbook.php"><img src="/img/0(4).jpg" class="pure-img"></a></div>
	<div><a href="./showbook.php"><img src="/img/0(3).jpg" class="pure-img"></a></div>
	<div><a href="./showbook.php"><img src="/img/0(6).jpg" class="pure-img"></a></div>
	<div><a href="./showbook.php"><img src="/img/0(5).jpg" class="pure-img"></a></div>
</div>

<div class="container">
<div class="pure-g">

<section class="pure-u-1-1">
	<h2>熱門小說</h2>
<div class="pure-g">
	<div class="pure-u-sm-1-3 pure-u-1">
	<div class="showcase-3">
	<a href="./showbook.php"><img src="/img/90(43).jpg"></a>
	<dl>
		<dt><cite><a href="./showbook.php">文藝王冠</a></cite></dt>
		<dd>作者:<a href="./author.php">李白不白</a></dd>
		<dd>分類:<a href="category.php">都市</a> / <a href="category.php">都市生活</a></dd>
		<dd>簡介:重生平行時空,加冕文藝王冠</dd>
	</dl>
	</div>
	</div>

	<div class="pure-u-sm-1-3 pure-u-1">
	<div class="showcase-3">
	<a href="./showbook.php"><img src="/img/90(43).jpg"></a>
	<dl>
		<dt><cite><a href="./showbook.php">文藝王冠</a></cite></dt>
		<dd>作者:<a href="./author.php">李白不白</a></dd>
		<dd>分類:<a href="category.php">都市</a> / <a href="category.php">都市生活</a></dd>
		<dd>簡介:重生平行時空,加冕文藝王冠</dd>
	</dl>
	</div>
	</div>

	<div class="pure-u-sm-1-3 pure-u-1">
	<div class="showcase-3">
	<a href="./showbook.php"><img src="/img/90(43).jpg"></a>
	<dl>
		<dt><cite><a href="./showbook.php">文藝王冠</a></cite></dt>
		<dd>作者:<a href="./author.php">李白不白</a></dd>
		<dd>分類:<a href="category.php">都市</a> / <a href="category.php">都市生活</a></dd>
		<dd>簡介:重生平行時空,加冕文藝王冠</dd>
	</dl>
	</div>
	</div>
</div>
</section>

<section class="pure-u-sm-2-3 pure-u-1">
	<h2>猜你喜歡</h2>
<div class="pure-g">
	<div class="pure-u-sm-1-2 pure-u-1">
	<div class="showcase-3">
	<a href="./showbook.php"><img src="/img/90(43).jpg"></a>
	<dl>
		<dt><cite><a href="./showbook.php">文藝王冠</a></cite></dt>
		<dd>作者:<a href="./author.php">李白不白</a></dd>
		<dd>分類:<a href="category.php">都市</a> / <a href="category.php">都市生活</a></dd>
		<dd>簡介:重生平行時空,加冕文藝王冠</dd>
	</dl>
	</div>
	</div>

	<div class="pure-u-sm-1-2 pure-u-1">
	<div class="showcase-3">
	<a href="./showbook.php"><img src="/img/90(43).jpg"></a>
	<dl>
		<dt><cite><a href="./showbook.php">文藝王冠</a></cite></dt>
		<dd>作者:<a href="./author.php">李白不白</a></dd>
		<dd>分類:<a href="category.php">都市</a> / <a href="category.php">都市生活</a></dd>
		<dd>簡介:重生平行時空,加冕文藝王冠</dd>
	</dl>
	</div>
	</div>
</div>
</section>

<section class="pure-u-sm-1-3 pure-u-1">
	<h2>限時免費</h2>
	<div class="indexcase-1">
	<ul>
		<li><a href="./category2.php">[鬼咒]</a><a href="./showbook.php">浪跡都市,笑傲紅塵</a></li>
		<li><a href="./category2.php">[抬棺匠]</a><a href="./showbook.php">我抬了三百多口棺材</a></li>
		<li><a href="./category2.php">[大官人]</a><a href="./showbook.php">盛世天下,四海來朝!</a></li>
		<li><a href="./category2.php">[武破九荒]</a><a href="./showbook.php">橫推萬敵,戰破九荒</a></li>
		<li><a href="./category2.php">[鬼咒]</a><a href="./showbook.php">浪跡都市,笑傲紅塵</a></li>
	</ul>
	</div>
</section>
</div>


<p class="text-center"><a href=""><img src="/img/0(12).jpg" class="pure-img"></a></p>


<h2 class="text-hidden">專屬榜</h2>
<div class="pure-g">
	<section class="pure-u-sm-1-3 pure-u-1">
	<h3>日銷榜</h3>
	<div class="ranking-2">
	<ol>
		<li>
		<div class="showcase-1">
		<a href=""><img src="/image/90(46).jpg"></a>
		<dl><dt><cite><a href="">完美人生</a></cite></dt>
			<dd>作者:<a href="">茅屋秋雨</a></dd>
			<dd>分類:<a href="">歷史</a> / <a href="">架空歷史</a></dd>
			<dd>簡介:文明從母系氏族開始</dd>
		</dl></div>
		</li>
		<li>
		<b><a href="./category2.php">幻想修仙</a></b>
		<cite><a href="./showbook.php">火影之最強震遁</a></cite>
		<i><a href="./author.php">茅屋秋雨</a></i>
		</li>
		<li>
		<b><a href="./category2.php">幻想修仙</a></b>
		<cite><a href="./showbook.php">火影之最強震遁</a></cite>
		<i><a href="./author.php">茅屋秋雨</a></i>
		</li>
		<li>
		<b><a href="./category2.php">幻想修仙</a></b>
		<cite><a href="./showbook.php">火影之最強震遁</a></cite>
		<i><a href="./author.php">茅屋秋雨</a></i>
		</li>
		<li>
		<b><a href="./category2.php">幻想修仙</a></b>
		<cite><a href="./showbook.php">火影之最強震遁</a></cite>
		<i><a href="./author.php">茅屋秋雨</a></i>
		</li>
		<li>
		<b><a href="./category2.php">幻想修仙</a></b>
		<cite><a href="./showbook.php">火影之最強震遁</a></cite>
		<i><a href="./author.php">茅屋秋雨</a></i>
		</li>
		<li>
		<b><a href="./category2.php">幻想修仙</a></b>
		<cite><a href="./showbook.php">火影之最強震遁</a></cite>
		<i><a href="./author.php">茅屋秋雨</a></i>
		</li>
		<li>
		<b><a href="./category2.php">幻想修仙</a></b>
		<cite><a href="./showbook.php">火影之最強震遁</a></cite>
		<i><a href="./author.php">茅屋秋雨</a></i>
		</li>
		<li>
		<b><a href="./category2.php">幻想修仙</a></b>
		<cite><a href="./showbook.php">火影之最強震遁</a></cite>
		<i><a href="./author.php">茅屋秋雨</a></i>
		</li>
		<li>
		<b><a href="./category2.php">幻想修仙</a></b>
		<cite><a href="./showbook.php">火影之最強震遁</a></cite>
		<i><a href="./author.php">茅屋秋雨</a></i>
		</li>
		<li>
		<b><a href="./category2.php">幻想修仙</a></b>
		<cite><a href="./showbook.php">火影之最強震遁</a></cite>
		<i><a href="./author.php">茅屋秋雨</a></i>
		</li>
	</ol>
	</div>
	</section>


	<section class="pure-u-sm-1-3 pure-u-1">
	<h3>日銷榜</h3>
	<div class="ranking-2">
	<ol>
		<li>
		<div class="showcase-1">
		<a href=""><img src="/img/90(46).jpg"></a>
		<dl>
			<dt><cite><a href="">完美人生</a></cite></dt>
			<dd>作者:<a href="">茅屋秋雨</a></dd>
			<dd>分類:<a href="">歷史</a> / <a href="">架空歷史</a></dd>
			<dd>簡介:文明從母系氏族開始</dd>
		</dl></div>
		</li>
		<li>
		<b><a href="./category2.php">幻想修仙</a></b>
		<cite><a href="./showbook.php">火影之最強震遁</a></cite>
		<i><a href="./author.php">茅屋秋雨</a></i>
		</li>
		<li>
		<b><a href="./category2.php">幻想修仙</a></b>
		<cite><a href="./showbook.php">火影之最強震遁</a></cite>
		<i><a href="./author.php">茅屋秋雨</a></i>
		</li>
		<li>
		<b><a href="./category2.php">幻想修仙</a></b>
		<cite><a href="./showbook.php">火影之最強震遁</a></cite>
		<i><a href="./author.php">茅屋秋雨</a></i>
		</li>
		<li>
		<b><a href="./category2.php">幻想修仙</a></b>
		<cite><a href="./showbook.php">火影之最強震遁</a></cite>
		<i><a href="./author.php">茅屋秋雨</a></i>
		</li>
		<li>
		<b><a href="./category2.php">幻想修仙</a></b>
		<cite><a href="./showbook.php">火影之最強震遁</a></cite>
		<i><a href="./author.php">茅屋秋雨</a></i>
		</li>
		<li>
		<b><a href="./category2.php">幻想修仙</a></b>
		<cite><a href="./showbook.php">火影之最強震遁</a></cite>
		<i><a href="./author.php">茅屋秋雨</a></i>
		</li>
		<li>
		<b><a href="./category2.php">幻想修仙</a></b>
		<cite><a href="./showbook.php">火影之最強震遁</a></cite>
		<i><a href="./author.php">茅屋秋雨</a></i>
		</li>
		<li>
		<b><a href="./category2.php">幻想修仙</a></b>
		<cite><a href="./showbook.php">火影之最強震遁</a></cite>
		<i><a href="./author.php">茅屋秋雨</a></i>
		</li>
		<li>
		<b><a href="./category2.php">幻想修仙</a></b>
		<cite><a href="./showbook.php">火影之最強震遁</a></cite>
		<i><a href="./author.php">茅屋秋雨</a></i>
		</li>
		<li>
		<b><a href="./category2.php">幻想修仙</a></b>
		<cite><a href="./showbook.php">火影之最強震遁</a></cite>
		<i><a href="./author.php">茅屋秋雨</a></i>
		</li>
	</ol>
	</div>
	</section>


	<section class="pure-u-sm-1-3 pure-u-1">
	<h3>日銷榜</h3>
	<div class="ranking-2">
	<ol>
		<li>
		<div class="showcase-1">
		<a href=""><img src="/img/90(46).jpg"></a>
		<dl>
			<dt><cite><a href="">完美人生</a></cite></dt>
			<dd>作者:<a href="">茅屋秋雨</a></dd>
			<dd>分類:<a href="">歷史</a> / <a href="">架空歷史</a></dd>
			<dd>簡介:文明從母系氏族開始</dd>
		</dl></div>
		</li>
		<li>
		<b><a href="./category2.php">幻想修仙</a></b>
		<cite><a href="./showbook.php">火影之最強震遁</a></cite>
		<i><a href="./author.php">茅屋秋雨</a></i>
		</li>
		<li>
		<b><a href="./category2.php">幻想修仙</a></b>
		<cite><a href="./showbook.php">火影之最強震遁</a></cite>
		<i><a href="./author.php">茅屋秋雨</a></i>
		</li>
		<li>
		<b><a href="./category2.php">幻想修仙</a></b>
		<cite><a href="./showbook.php">火影之最強震遁</a></cite>
		<i><a href="./author.php">茅屋秋雨</a></i>
		</li>
		<li>
		<b><a href="./category2.php">幻想修仙</a></b>
		<cite><a href="./showbook.php">火影之最強震遁</a></cite>
		<i><a href="./author.php">茅屋秋雨</a></i>
		</li>
		<li>
		<b><a href="./category2.php">幻想修仙</a></b>
		<cite><a href="./showbook.php">火影之最強震遁</a></cite>
		<i><a href="./author.php">茅屋秋雨</a></i>
		</li>
		<li>
		<b><a href="./category2.php">幻想修仙</a></b>
		<cite><a href="./showbook.php">火影之最強震遁</a></cite>
		<i><a href="./author.php">茅屋秋雨</a></i>
		</li>
		<li>
		<b><a href="./category2.php">幻想修仙</a></b>
		<cite><a href="./showbook.php">火影之最強震遁</a></cite>
		<i><a href="./author.php">茅屋秋雨</a></i>
		</li>
		<li>
		<b><a href="./category2.php">幻想修仙</a></b>
		<cite><a href="./showbook.php">火影之最強震遁</a></cite>
		<i><a href="./author.php">茅屋秋雨</a></i>
		</li>
		<li>
		<b><a href="./category2.php">幻想修仙</a></b>
		<cite><a href="./showbook.php">火影之最強震遁</a></cite>
		<i><a href="./author.php">茅屋秋雨</a></i>
		</li>
		<li>
		<b><a href="./category2.php">幻想修仙</a></b>
		<cite><a href="./showbook.php">火影之最強震遁</a></cite>
		<i><a href="./author.php">茅屋秋雨</a></i>
		</li>
	</ol>
	</div>
	</section>

<div class="pure-u-1">
	<p class="read-more"><a href="./ranking.php">查看更多 􏰀<i class="fa fa-chevron-right" aria-hidden="true"></i></a></p>
</div>
</div>



<h2 class="text-hidden">男生精選</h2>
<div class="pure-g">

	<section class="pure-u-sm-1-3 pure-u-1">
	<h3>玄幻奇幻</h3>

	<div class="showcase-3">
	<a href="./showbook.php"><img src="/img/90(43).jpg"></a>
	<dl>
		<dt><cite><a href="./showbook.php">文藝王冠</a></cite></dt>
		<dd>作者:<a href="./author.php">李白不白</a></dd>
		<dd>分類:<a href="category.php">都市</a> / <a href="category.php">都市生活</a></dd>
		<dd>簡介:重生平行時空,加冕文藝王冠</dd>
	</dl>
	</div>

	<div class="indexcase-2">
	<ul>
		<li><a href="./category2.php">[鬼咒]</a><a href="./showbook.php">浪跡都市,笑傲紅塵</a></li>
		<li><a href="./category2.php">[抬棺匠]</a><a href="./showbook.php">我抬了三百多口棺材</a></li>
		<li><a href="./category2.php">[大官人]</a><a href="./showbook.php">盛世天下,四海來朝!</a></li>
		<li><a href="./category2.php">[武破九荒]</a><a href="./showbook.php">橫推萬敵,戰破九荒</a></li>
		<li><a href="./category2.php">[鬼咒]</a><a href="./showbook.php">浪跡都市,笑傲紅塵</a></li>
	</ul>
	</div>
	</section>

	<section class="pure-u-sm-1-3 pure-u-1">
	<h3>武俠仙俠</h3>

	<div class="showcase-3">
	<a href="./showbook.php"><img src="/img/90(43).jpg"></a>
	<dl>
		<dt><cite><a href="./showbook.php">文藝王冠</a></cite></dt>
		<dd>作者:<a href="./author.php">李白不白</a></dd>
		<dd>分類:<a href="category.php">都市</a> / <a href="category.php">都市生活</a></dd>
		<dd>簡介:重生平行時空,加冕文藝王冠</dd>
	</dl>
	</div>

	<div class="indexcase-2">
	<ul>
		<li><a href="./category2.php">[鬼咒]</a><a href="./showbook.php">浪跡都市,笑傲紅塵</a></li>
		<li><a href="./category2.php">[抬棺匠]</a><a href="./showbook.php">我抬了三百多口棺材</a></li>
		<li><a href="./category2.php">[大官人]</a><a href="./showbook.php">盛世天下,四海來朝!</a></li>
		<li><a href="./category2.php">[武破九荒]</a><a href="./showbook.php">橫推萬敵,戰破九荒</a></li>
		<li><a href="./category2.php">[鬼咒]</a><a href="./showbook.php">浪跡都市,笑傲紅塵</a></li>
	</ul>
	</div>
	</section>

	<section class="pure-u-sm-1-3 pure-u-1">
	<h3>都市言情</h3>

	<div class="showcase-3">
	<a href="./showbook.php"><img src="/img/90(43).jpg"></a>
	<dl>
		<dt><cite><a href="./showbook.php">文藝王冠</a></cite></dt>
		<dd>作者:<a href="./author.php">李白不白</a></dd>
		<dd>分類:<a href="category.php">都市</a> / <a href="category.php">都市生活</a></dd>
		<dd>簡介:重生平行時空,加冕文藝王冠</dd>
	</dl>
	</div>

	<div class="indexcase-2">
	<ul>
		<li><a href="./category2.php">[鬼咒]</a><a href="./showbook.php">浪跡都市,笑傲紅塵</a></li>
		<li><a href="./category2.php">[抬棺匠]</a><a href="./showbook.php">我抬了三百多口棺材</a></li>
		<li><a href="./category2.php">[大官人]</a><a href="./showbook.php">盛世天下,四海來朝!</a></li>
		<li><a href="./category2.php">[武破九荒]</a><a href="./showbook.php">橫推萬敵,戰破九荒</a></li>
		<li><a href="./category2.php">[鬼咒]</a><a href="./showbook.php">浪跡都市,笑傲紅塵</a></li>
	</ul>
	</div>
	</section>

	<section class="pure-u-sm-1-3 pure-u-1">
	<h3>歷史軍事</h3>

	<div class="showcase-3">
	<a href="./showbook.php"><img src="/img/90(43).jpg"></a>
	<dl>
		<dt><cite><a href="./showbook.php">文藝王冠</a></cite></dt>
		<dd>作者:<a href="./author.php">李白不白</a></dd>
		<dd>分類:<a href="category.php">都市</a> / <a href="category.php">都市生活</a></dd>
		<dd>簡介:重生平行時空,加冕文藝王冠</dd>
	</dl>
	</div>

	<div class="indexcase-2">
	<ul>
		<li><a href="./category2.php">[鬼咒]</a><a href="./showbook.php">浪跡都市,笑傲紅塵</a></li>
		<li><a href="./category2.php">[抬棺匠]</a><a href="./showbook.php">我抬了三百多口棺材</a></li>
		<li><a href="./category2.php">[大官人]</a><a href="./showbook.php">盛世天下,四海來朝!</a></li>
		<li><a href="./category2.php">[武破九荒]</a><a href="./showbook.php">橫推萬敵,戰破九荒</a></li>
		<li><a href="./category2.php">[鬼咒]</a><a href="./showbook.php">浪跡都市,笑傲紅塵</a></li>
	</ul>
	</div>
	</section>

	<section class="pure-u-sm-1-3 pure-u-1">
	<h3>遊戲體育</h3>

	<div class="showcase-3">
	<a href="./showbook.php"><img src="/img/90(43).jpg"></a>
	<dl>
		<dt><cite><a href="./showbook.php">文藝王冠</a></cite></dt>
		<dd>作者:<a href="./author.php">李白不白</a></dd>
		<dd>分類:<a href="category.php">都市</a> / <a href="category.php">都市生活</a></dd>
		<dd>簡介:重生平行時空,加冕文藝王冠</dd>
	</dl>
	</div>

	<div class="indexcase-2">
	<ul>
		<li><a href="./category2.php">[鬼咒]</a><a href="./showbook.php">浪跡都市,笑傲紅塵</a></li>
		<li><a href="./category2.php">[抬棺匠]</a><a href="./showbook.php">我抬了三百多口棺材</a></li>
		<li><a href="./category2.php">[大官人]</a><a href="./showbook.php">盛世天下,四海來朝!</a></li>
		<li><a href="./category2.php">[武破九荒]</a><a href="./showbook.php">橫推萬敵,戰破九荒</a></li>
		<li><a href="./category2.php">[鬼咒]</a><a href="./showbook.php">浪跡都市,笑傲紅塵</a></li>
	</ul>
	</div>
	</section>

	<section class="pure-u-sm-1-3 pure-u-1">
	<h3>科幻靈異</h3>

	<div class="showcase-3">
	<a href="./showbook.php"><img src="/img/90(43).jpg"></a>
	<dl>
		<dt><cite><a href="./showbook.php">文藝王冠</a></cite></dt>
		<dd>作者:<a href="./author.php">李白不白</a></dd>
		<dd>分類:<a href="category.php">都市</a> / <a href="category.php">都市生活</a></dd>
		<dd>簡介:重生平行時空,加冕文藝王冠</dd>
	</dl>
	</div>

	<div class="indexcase-2">
	<ul>
		<li><a href="./category2.php">[鬼咒]</a><a href="./showbook.php">浪跡都市,笑傲紅塵</a></li>
		<li><a href="./category2.php">[抬棺匠]</a><a href="./showbook.php">我抬了三百多口棺材</a></li>
		<li><a href="./category2.php">[大官人]</a><a href="./showbook.php">盛世天下,四海來朝!</a></li>
		<li><a href="./category2.php">[武破九荒]</a><a href="./showbook.php">橫推萬敵,戰破九荒</a></li>
		<li><a href="./category2.php">[鬼咒]</a><a href="./showbook.php">浪跡都市,笑傲紅塵</a></li>
	</ul>
	</div>
	</section>

</div>



<h2 class="text-hidden">女生必看</h2>
<div class="pure-g">

	<section class="pure-u-sm-1-3 pure-u-1">
	<h3>重磅推薦</h3>

	<div class="showcase-3">
	<a href="./showbook.php"><img src="/img/90(43).jpg"></a>
	<dl>
		<dt><cite><a href="./showbook.php">文藝王冠</a></cite></dt>
		<dd>作者:<a href="./author.php">李白不白</a></dd>
		<dd>分類:<a href="category.php">都市</a> / <a href="category.php">都市生活</a></dd>
		<dd>簡介:重生平行時空,加冕文藝王冠</dd>
	</dl>
	</div>

	<div class="indexcase-2">
	<ul>
		<li><a href="./category2.php">[鬼咒]</a><a href="./showbook.php">浪跡都市,笑傲紅塵</a></li>
		<li><a href="./category2.php">[抬棺匠]</a><a href="./showbook.php">我抬了三百多口棺材</a></li>
		<li><a href="./category2.php">[大官人]</a><a href="./showbook.php">盛世天下,四海來朝!</a></li>
		<li><a href="./category2.php">[武破九荒]</a><a href="./showbook.php">橫推萬敵,戰破九荒</a></li>
		<li><a href="./category2.php">[鬼咒]</a><a href="./showbook.php">浪跡都市,笑傲紅塵</a></li>
	</ul>
	</div>
	</section>

	<section class="pure-u-sm-1-3 pure-u-1">
	<h3>白金作家</h3>

	<div class="showcase-3">
	<a href="./showbook.php"><img src="/img/90(43).jpg"></a>
	<dl>
		<dt><cite><a href="./showbook.php">文藝王冠</a></cite></dt>
		<dd>作者:<a href="./author.php">李白不白</a></dd>
		<dd>分類:<a href="category.php">都市</a> / <a href="category.php">都市生活</a></dd>
		<dd>簡介:重生平行時空,加冕文藝王冠</dd>
	</dl>
	</div>

	<div class="indexcase-2">
	<ul>
		<li><a href="./category2.php">[鬼咒]</a><a href="./showbook.php">浪跡都市,笑傲紅塵</a></li>
		<li><a href="./category2.php">[抬棺匠]</a><a href="./showbook.php">我抬了三百多口棺材</a></li>
		<li><a href="./category2.php">[大官人]</a><a href="./showbook.php">盛世天下,四海來朝!</a></li>
		<li><a href="./category2.php">[武破九荒]</a><a href="./showbook.php">橫推萬敵,戰破九荒</a></li>
		<li><a href="./category2.php">[鬼咒]</a><a href="./showbook.php">浪跡都市,笑傲紅塵</a></li>
	</ul>
	</div>
	</section>


	<section class="pure-u-sm-1-3 pure-u-1">
	<h3>新人新作</h3>

	<div class="showcase-3">
	<a href="./showbook.php"><img src="img/90(43).jpg"></a>
	<dl>
		<dt><cite><a href="./showbook.php">文藝王冠</a></cite></dt>
		<dd>作者:<a href="./author.php">李白不白</a></dd>
		<dd>分類:<a href="category.php">都市</a> / <a href="category.php">都市生活</a></dd>
		<dd>簡介:重生平行時空,加冕文藝王冠</dd>
	</dl>
	</div>

	<div class="indexcase-2">
	<ul>
		<li><a href="./category2.php">[鬼咒]</a><a href="./showbook.php">浪跡都市,笑傲紅塵</a></li>
		<li><a href="./category2.php">[抬棺匠]</a><a href="./showbook.php">我抬了三百多口棺材</a></li>
		<li><a href="./category2.php">[大官人]</a><a href="./showbook.php">盛世天下,四海來朝!</a></li>
		<li><a href="./category2.php">[武破九荒]</a><a href="./showbook.php">橫推萬敵,戰破九荒</a></li>
		<li><a href="./category2.php">[鬼咒]</a><a href="./showbook.php">浪跡都市,笑傲紅塵</a></li>
	</ul>
	</div>
	</section>

</div>




<h2 class="text-hidden">小說風向榜</h2>
<div class="pure-g">

<section class="pure-u-sm-1-2 pure-u-1">
	<h3>男生風向榜</h3>

	<div class="showcase-1">
	<a href="./showbook.php"><img src="/img/90(42).jpg"></a>
	<dl>
		<dt><cite><a href="./showbook.php">崛起之第三帝國</a></cite></dt>
		<dd>作者:<a href="./author.php">大羅羅</a></dd>
		<dd>德意志第二帝國的戰士在漫長的塹壕中拼盡全力掙扎,不可一世...</dd>
	</dl>
	</div>

	<div class="indexcase-1">
	<ul>
		<li>
			<b><a href="./category2.php">[鬼咒]</a></b>
			<cite><a href="./showbook.php">浪跡都市,笑傲紅塵</a></cite>
			<i><a href="./author.php">茅屋秋雨</a></i></li>
		<li>
			<b><a href="./category2.php">[抬棺匠]</a></b>
			<cite><a href="./showbook.php">我抬了三百多口棺材</a></cite>
			<i><a href="./author.php">茅屋秋雨</a></i></li>
		<li>
			<b><a href="./category2.php">[大官人]</a></b>
			<cite><a href="./showbook.php">盛世天下,四海來朝!</a></cite>
			<i><a href="./author.php">茅屋秋雨</a></i></li>
		<li>
			<b><a href="./category2.php">[武破九荒]</a></b>
			<cite><a href="./showbook.php">橫推萬敵,戰破九荒</a></cite>
			<i><a href="./author.php">茅屋秋雨</a></i></li>
		<li>
			<b><a href="./category2.php">[鬼咒]</a></b>
			<cite><a href="./showbook.php">浪跡都市,笑傲紅塵</a></cite>
			<i><a href="./author.php">茅屋秋雨</a></i></li>
	</ul>
	</div>
	<p class="read-more"><a href="./ranking.php">查看更多 􏰀<i class="fa fa-chevron-right" aria-hidden="true"></i></a></p>
</section>

<section class="pure-u-sm-1-2 pure-u-1">
	<h3>女生風向榜</h3>

	<div class="showcase-1">
	<a href="./showbook.php"><img src="/img/90(42).jpg"></a>
	<dl>
		<dt><cite><a href="./showbook.php">崛起之第三帝國</a></cite></dt>
		<dd>作者:<a href="./author.php">大羅羅</a></dd>
		<dd>德意志第二帝國的戰士在漫長的塹壕中拼盡全力掙扎,不可一世...</dd>
	</dl>
	</div>

	<div class="indexcase-1">
	<ul>
		<li>
			<b><a href="./category2.php">[鬼咒]</a></b>
			<cite><a href="./showbook.php">浪跡都市,笑傲紅塵</a></cite>
			<i><a href="./author.php">茅屋秋雨</a></i></li>
		<li>
			<b><a href="./category2.php">[抬棺匠]</a></b>
			<cite><a href="./showbook.php">我抬了三百多口棺材</a></cite>
			<i><a href="./author.php">茅屋秋雨</a></i></li>
		<li>
			<b><a href="./category2.php">[大官人]</a></b>
			<cite><a href="./showbook.php">盛世天下,四海來朝!</a></cite>
			<i><a href="./author.php">茅屋秋雨</a></i></li>
		<li>
			<b><a href="./category2.php">[武破九荒]</a></b>
			<cite><a href="./showbook.php">橫推萬敵,戰破九荒</a></cite>
			<i><a href="./author.php">茅屋秋雨</a></i></li>
		<li>
			<b><a href="./category2.php">[鬼咒]</a></b>
			<cite><a href="./showbook.php">浪跡都市,笑傲紅塵</a></cite>
			<i><a href="./author.php">茅屋秋雨</a></i></li>
	</ul>
	</div>

	<p class="read-more"><a href="./ranking.php">查看更多 􏰀<i class="fa fa-chevron-right" aria-hidden="true"></i></a></p>
</section>
</div><!-- /.pure-g -->



<div class="pure-g">
<h2 class="pure-u-1 text-bn">精選專題</h2>
	<div class="pure-u-sm-1-4 pure-u-1-2"><a href=""><img src="/img/0(4).jpg" class="pure-img"></a></div>
	<div class="pure-u-sm-1-4 pure-u-1-2"><a href=""><img src="/img/0(3).jpg" class="pure-img"></a></div>
	<div class="pure-u-sm-1-4 pure-u-1-2"><a href=""><img src="/img/0(6).jpg" class="pure-img"></a></div>
	<div class="pure-u-sm-1-4 pure-u-1-2"><a href=""><img src="/img/0(5).jpg" class="pure-img"></a></div>

	<div class="pure-u-1">
	<p class="read-more"><a href="./ranking.php">查看更多 􏰀<i class="fa fa-chevron-right" aria-hidden="true"></i></a></p>
	</div>

<h2 class="text-bn">公告專區</h2>
	<p class="text-center"><a href=""><img src="/img/0.jpg" class="pure-img"></a></p>
</div>

</div><!-- /.container -->
</main>

