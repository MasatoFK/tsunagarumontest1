<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<!--[if (gte IE 9)|!(IE)]><!--><html lang="zh"><!--<![endif]-->
<head prefix="og: http;//ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#">

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title><?= $this->fetch('title') ?></title>

	<meta content="{$title->head} - {$name}" name="title">
	<meta content="{$title->description}" name="description">
	<meta content="{$title->keyword}" name="keywords">
	<meta content="index, follow" name="robots">
	<meta content="all" name="googlebot">
	<meta content="all" name="baiduspider">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta http-equiv="imagetoolbar"    content="no">
	<meta name="format-detection"      content="telephone=no">

	<link rel="shortcut icon" href="/img/favicon.png">
	<link rel="Bookmark" href="/img/favicon.png">
	<link rel="apple-touch-icon" href="/img/icon_57.png">

	<link rel="canonical" href="http://tsunagarumon.com">


<!-- Framework, Icons -->
	<link href="/css/pure-min.css" rel="stylesheet">
	<link href="/css/grids-responsive.css" rel="stylesheet">
	<link href="/css/font-awesome.min.css" rel="stylesheet">

<!-- Custom styles -->
	<link rel="stylesheet" href="/css/styles.css">

	<link rel="stylesheet" href="/slick/slick.css">
	<link rel="stylesheet" href="/slick/slick-theme.css">
</head>

<header role="banner"><div class="container pure-g">
	<h1 id="siteLogo" class="pure-u-md-1-5 pure-u-1-3"><a href="/">TSUNAGARUMON</a></h1>

	<form class="pure-u-md-3-5 pure-u-1-3">
		<input type="text" class="search-input">
		<button type="submit" class="fa fa-search search-button"></button>
	</form>

	<ul class="pure-u-md-1-5 pure-u-1-3">
		<li><a href="/users/logout">註銷</a></li>
		<li><a href="bookshelf.php">書架</a></li>
	</ul>
</div></header>



<nav class="global" role="navigation"><div class="container">

	<ul class="global-nav-1">
		<li><i class="fa fa-bars" aria-hidden="true"></i>作品分類
			<ul class="category-list">
				<li><a href="category.php">玄幻</a></li>
				<li><a href="category.php">奇幻</a></li>
				<li><a href="category.php">武俠</a></li>
				<li><a href="category.php">仙俠</a></li>
				<li><a href="category.php">都市</a></li>
				<li><a href="category.php">職場</a></li>
				<li><a href="category.php">軍事</a></li>
				<li><a href="category.php">歷史</a></li>
				<li><a href="category.php">遊戲</a></li>
				<li><a href="category.php">體育</a></li>
				<li><a href="category.php">科幻</a></li>
				<li><a href="category.php">靈異</a></li>
				<li><a href="category.php">女生網</a></li>
				<li><a href="category.php">二次元</a></li>
			</ul>
		</li>
		<li><a href="bookstore.php">書庫</a></li>
		<li><a href="category.php">分類</a></li>
		<li><a href="ranking.php">排行</a></li>
		<li><a href="bookunit.php">完本</a></li>
		<li><a href="free.php">限免</a></li>
		<li><a href="bookshop.php">書坊</a></li>
	</ul>


	<div class="global-nav-2-fixed">
		<div class="group-nav01">
			<p class="logo"><a href="/">tsunagarumon</a></p>
			<ul>
				<li><a href="bookstore.php">書庫</a></li>
				<li><a href="category.php">分類</a></li>
				<li><a href="ranking.php">排行</a></li>
				<li><a href="bookunit.php">完本</a></li>
				<li><a href="free.php">限免</a></li>
				<li><a href="ranking-boy.php">男生</a></li>
				<li><a href="ranking-girl.php">女生</a></li>
				<li><a href="bookshop.php">書坊</a></li>
			</ul>
		</div>

		<div class="group-nav02">

			<div class="login-mypage">
				<a href="bookshelf.php"><i class="fa fa-book" aria-hidden="true"></i>我的書架</a>
			</div>

			<div class="login-link">
				<a href="login.php">登錄</a>
				<a href="register.php">註冊</a>
			</div>


			<div id="sb-search" class="sb-search">
				<form>
					<input class="sb-search-input" placeholder="快速搜索、找書、找武俠" type="text" value="" name="search" id="search">
					<input class="sb-search-submit" type="submit" value="">
					<span class="sb-icon-search"></span>
				</form>
			</div>
		</div>

	</div></div><!-- /.global-nav-2-fixed -->

</nav>

<!-- この部分のみ表示内容を変更する -->
<?php echo $this->fetch('content'); ?>

<p id="page-top"><i class="fa fa-angle-double-up" aria-hidden="true"></i></p>


<footer role="contentinfo"><div class="container pure-g">

	<div class="pure-u-1-4">
		<ul class="pure-g">
			<li class="pure-u-1-3"><a href="login.html" class="pure-button">登錄</a></li>
			<li class="pure-u-1-3"><a href="register.html" class="pure-button">註冊</a></li>
			<li class="pure-u-1-3"><a href="bookshelf.html" class="pure-button">書架</a></li>
		</ul>
	</div>

	<div class="pure-u-3-4">
		<form class="pure-form">
			<input type="text" class="search-input">
			<button type="submit" class="fa fa-search search-button pure-button"></button>
		</form>
	</div>

	<div class="pure-u-1">
		<p class="text-center">
			<a href="">極速版</a>
			<span>:</span>
			<a href="/">炫彩版</a>
			<span>:</span>
			<a href="help.html">幫助說明</a>
			<span>:</span>
			<a href="feedback.html">回饋意見</a>
		</p>
	</div>

	<div class="pure-u-1">
		<p id="siteLogo" class="text-right"><a href="./index.html">TSUNAGARUMON</a>
		</p>
	</div>
</div></footer>



<script src="/js/jquery-3.1.1.min.js"></script>
<script src="/js/jquery-migrate-1.4.1.min.js"></script>
<script src="/js/classie.js"></script>
<script src="/js/uisearch.js"></script>
<script src="/js/script.js"></script>

<!--[if lt IE 9]>
<script src="/js/html5.js"></script>
<script src="/js/IE9.js"></script>
<script src="/js/css3-mediaqueries.js"></script>
<![endif]-->

<script src="slick/slick.min.js"></script>
<script>
	$(document).ready(function(){
		$('.main-slick').slick({
			autoplay: true,
			autoplaySpeed: 3000,
			arrows: false,
			centerMode: true,
			dots:true,
			infinite: true,
			slidesToShow: 1,
			speed: 500,
			variableWidth: true,
			responsive: [
				{
					breakpoint: 480,
					settings: {
						variableWidth: false
					}
				}
			]
		});
	});
</script>

</body>
</html>
