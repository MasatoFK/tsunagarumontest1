<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * WorkCategories Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Works
 * @property \Cake\ORM\Association\BelongsTo $Categories
 *
 * @method \App\Model\Entity\WorkCategory get($primaryKey, $options = [])
 * @method \App\Model\Entity\WorkCategory newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\WorkCategory[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\WorkCategory|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\WorkCategory patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\WorkCategory[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\WorkCategory findOrCreate($search, callable $callback = null)
 */
class WorkCategoriesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('work_categories');
        $this->displayField('work_id');
        $this->primaryKey(['work_id', 'category_id']);

        $this->belongsTo('Works', [
            'foreignKey' => 'work_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Categories', [
            'foreignKey' => 'category_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['work_id'], 'Works'));
        $rules->add($rules->existsIn(['category_id'], 'Categories'));

        return $rules;
    }
}
